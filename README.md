# Botany 2023 Workshop on Phylogenetic Networks

This GitLab repository hosts materials for the "Estimating Phylogenetic Support Using RAWR" workshop session.

## Presentation slides

A copy of the workshop session slides can be found here: [rawr-software-workshop-2023-export.pdf](rawr-software-workshop-2023-export.pdf).

## Extended RAWR tutorial document

An extended RAWR tutorial is provided: [extended-rawr-tutorial.pdf](extended-rawr-tutorial.pdf). The workshop session will work through a few of the tutorial sections.

## VM image file

An Ubuntu virtual machine (VM) is provided in Open Virtual Appliance (OVA) format: [rawr-demo-vm.ova](rawr-demo-vm.ova). The file can be imported by popular VM software packages, including [VirtualBox](https://www.virtualbox.org) and [VMWare Fusion](https://www.vmware.com/products/fusion.html). The image has been preconfigured with all relevant dependencies for running the RAWR standalone desktop client.

**NOTE:** the Ubuntu VM image can only be run on macOS hosts with Apple silicon processors (i.e., relatively recent Apple computers and devices).

After importing and starting the VM, you can login to the preconfigured Ubuntu Linux instance using the following login info:

- Username: demo
- Password: rawrsoftware

## RAWR software: beta version July 2023

A beta version July 2023 of the RAWR software package is provided here: [RAWR-web-software-july-2023-beta.zip](RAWR-web-software-july-2023-beta.zip). The current beta version has been developed and tested for popular desktop operating systems including macOS, Microsoft Windows, and Linux; Galaxy-based and web app implementations are included as well. The software is also installed in the Ubuntu VM image. 

## Additional datasets from Wang et al. 2021

Additional datasets to analyze with RAWR are publicly available under open copyleft licenses: [https://gitlab.msu.edu/liulab/rawr-study-datasets-and-scripts](https://gitlab.msu.edu/liulab/rawr-study-datasets-and-scripts). The datasets come from the study of Wang et al. 2021.
